#include<stdio.h>
int
main ()
{

  printf ("PROGRAM TO PRINT LEAP YEAR BETWEEN 1980 AND 2020\n");
  int y = 1980;
  while (y <= 2020)
    {
      if (y % 4 == 0)
	printf ("%d\n", y);
      y++;
    }

  return 0;
}