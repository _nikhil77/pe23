#include <stdio.h>
#include<math.h>

float get_radius()
{
       float radius;
        printf("enter the radius\n");
        scanf("%f",&radius);
        return radius;
}

float compute_area(float radius)
{
       return M_PI*radius*radius;
}

float output(float radius, float area)
{
       printf("the area of the circle with radius=%f is %f \n",radius,area);
}

int main()
{
float radius,area;
radius = get_radius();
area = compute_area(radius);
output(radius,area);
}
